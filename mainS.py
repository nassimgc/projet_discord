import discord
from discord.ext import commands
from random import *

description = ":arrow_forward: Robot crée par Miguel et Nassim\n\nTapez !help pour plus d'aide."

bot = commands.Bot(command_prefix='!', description=description)
#Supprimer la commande help par défaut car sinon erreur car redéfinition de la méthode help
bot.remove_command("help")
bot.game = discord.Game(name='Robot crée par Miguel et Nassim')


@bot.event
async def on_ready():
    print('Logged in as')
    print(bot.user.name)
    print(bot.user.id)
    print(str(bot.game))
    print('------')


@bot.command(pass_context=True)
async def help(ctx):
    await ctx.send(':arrow_right:	-	**!de + chiffres entre 1 et 6** : Lance un dé'
        '\n:arrow_right:	-	**!help** : Affiche l\'aide'
        '\n:arrow_right:	-	**Tapez pierre ou feuille ou ciseaux** :  Lancer le pierre-feuille-ciseaux'
        '\n:arrow_right:	-	**!desc** : Description du bot.')

@bot.command(pass_context=True)
async def desc(ctx):
    await ctx.send(bot.description)

@bot.command(pass_context=True)

async def de(ctx,number: int):
    """Lancé de dés."""
    try:
        tmp = randint(1, 6)

        if number > 6 or number <= 0 :
            await ctx.send(':game_die: {0.author.mention} ton chiffre doit être entre 1 et 6:game_die:'.format(ctx))
        else : 
            if number == tmp:
                await ctx.send(':game_die: Bien joué {0.author.mention} :game_die:'.format(ctx))

            else:
                await ctx.send(':game_die: Dommage le chiffre était {} :game_die:'.format(tmp)+"\n Dommage {0.author.mention}".format(ctx))

    except Exception:
        await ctx.send('Error !')

        return


async def pfc(message):
    try:
        tmp = randint(1, 3)
        if tmp == 1:
            #Ciseaux
            await message.channel.send(':scissors:')
            if message.content.lower()=="pierre":
                await message.channel.send('Bien joué {0.author.mention}'.format(message))

            elif message.content.lower()=="ciseaux":
                await message.channel.send('Egalité')
                
            else:
                await message.channel.send('Perdu réessayes {0.author.mention}'.format(message))
        
        elif tmp == 2:
            #Pierre
            await message.channel.send(':punch:')

            if message.content.lower()=="pierre":               
                await message.channel.send('Egalité')

            elif message.content.lower()=="ciseaux":
                await message.channel.send('Perdu réessayes {0.author.mention}'.format(message))
            
            else:    
                await message.channel.send('Bien joué {0.author.mention}'.format(message))
        
        else:
            #Feuille
            await message.channel.send(':raised_hand:')

            if message.content.lower()=="pierre":
                await message.channel.send('Perdu réessayes {0.author.mention}'.format(message))
            
            elif message.content.lower()=="ciseaux":
                await message.channel.send('Bien joué {0.author.mention}'.format(message))
            
            else:
                await message.channel.send('Egalité')
    
    except Exception:
        await message.channel.send('Error !')


@bot.event
async def on_message(message):
    liste = ["pierre","feuille","ciseaux"]
    
    if message.content.lower() in liste:
        await pfc(message)
    
    else:
        await bot.process_commands(message)

bot.run("ODUyMTg0NTk2OTU5NDYxNDI3.YMDIwA.Oo6tOKsYEJaJ4Izbc7rrE9ZlL9I")